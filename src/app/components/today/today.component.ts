import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { formatDate } from '../../utils/utils';

@Component({
  selector: 'app-today',
  templateUrl: './today.component.html',
  styleUrls: ['./today.component.css']
})
export class TodayComponent implements OnInit {

  @Input() city!: string;
  @Input() icon!: string;
  @Input() temp!: number;
  @Input() description!: string;
  today: string;
  @Output() handleSearch: EventEmitter<boolean> = new EventEmitter();
  @Output() getUserLocation: EventEmitter<void> = new EventEmitter();

  constructor() {
    this.today = formatDate(new Date());
  }

  ngOnInit(): void {
  }

}
