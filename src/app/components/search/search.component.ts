import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CitiesService } from '../../services/cities.service';
import { LocationService } from 'src/app/services/location.service';
import { City } from '../../interfaces/interfaces';
import { formatString } from '../../utils/utils';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  cities!: Array<City>;
  errorCityNotFound: boolean = false;
  @Output() handleSearch: EventEmitter<boolean> = new EventEmitter();
  @Output() searchCityWeathers: EventEmitter<any> = new EventEmitter();

  constructor(private citiesServices: CitiesService,
              private locationServices: LocationService) {
    this.cities = this.citiesServices.cities;
  }

  ngOnInit(): void {
  }

  async onSubmit(values: any) {
    if (values.city === '' || values.city.trim().length === 0) return;
    if (this.cities.some(city => formatString(city.name) === formatString(values.city))) return;
    try {
      const resp = await this.locationServices.getLatLngCity(values.city);
      if (resp.latitude !== 0 && resp.longitude !== 0) {
        const city: City = { 'name': values.city, 'latitude': resp.latitude, 'longitude': resp.longitude };
        this.searchCityWeathers.emit(city);
        this.citiesServices.add(city);
      } else {
        this.errorCityNotFound = true;
      }
    } catch (err) {
      console.log(err);
    }
  }

  searchCity(city: City) {
    this.searchCityWeathers.emit(city);
  }

  removeCity(city: City) {
    this.citiesServices.delete(city);
  }

  close() {
    this.handleSearch.emit(false);
  }

}
