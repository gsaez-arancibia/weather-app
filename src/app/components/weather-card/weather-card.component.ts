import { Component, Input, OnInit } from '@angular/core';
import { formatDate } from '../../utils/utils';

@Component({
  selector: 'app-weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.css']
})
export class WeatherCardComponent implements OnInit {

  @Input() date!: any;
  @Input() icon!: string;
  @Input() tempMin!: number;
  @Input() tempMax!: number;

  constructor() { }

  ngOnInit(): void {
    this.date = formatDate(new Date(this.date * 1000));
  }

}
