import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

const BASE_URL_WEATHER = 'https://api.openweathermap.org/data/2.5';
const API_KEY = 'e059d021ec121e9af9b1d2c69efe50c4';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http: HttpClient) { }

  getCurrentWeather(city: string): Promise<any> {
    return this.http.get(`${ BASE_URL_WEATHER }/weather?q=${ city }&appid=${ API_KEY }&units=metric&lang=es`).toPromise();
  }

  getAllWeathers(latitude: number, longitude: number): Promise<any> {
    const url = `${ BASE_URL_WEATHER }/onecall?lat=${ latitude }&lon=${ longitude }&exclude=minutely,hourly,alerts&appid=${ API_KEY }&units=metric&lang=es`;
    return this.http.get(url)
      .pipe(
        map((response: any) => {
          return {
            'current': response.current, 'daily': response.daily.filter((e: any, i: number) => i !== 0)
          }
        }),
        catchError(err => throwError(err))
      ).toPromise();
  }

}
