# Weather App - Angular

<img src="/src/assets/images/imagen1-min.jpg?raw=true" width="45%">
<img src="/src/assets/images/imagen2-min.jpg?raw=true" width="45%">
<img src="/src/assets/images/imagen3-min.jpg?raw=true" width="45%" height="500">
<img src="/src/assets/images/imagen4-min.jpg?raw=true" width="45%" height="500">
<img src="/src/assets/images/imagen5-min.jpg?raw=true" width="45%" height="500">

### Pre-requisitos 📋

* Angular CLI
* Node.js - npm

### Instalación 🔧

_En el directorio del proyecto ingresar el siguiente comando a través de una terminal_

* npm install

## Despliegue 📦

_Ejecutar el siguiente comando para correr el proyecto_

* ng serve

_Cuando termine, ingresar a http://localhost:4200_

## Construido con 🛠️

* [Angular](https://angular.io/docs)
* [API-WEATHER](https://openweathermap.org/api)
* [API-LOCATION](https://nominatim.org/release-docs/develop/api/Overview/)